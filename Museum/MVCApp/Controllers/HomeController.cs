﻿using DataAccessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCApp.Controllers
{
    public class HomeController : Controller
    {
        /*I don't use try catch and using because my all process are in the DAO layer*/

        /// <summary>
        /// This action shows museum List
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            List<BusinessObjects.MuseumDTO> museumList = new List<BusinessObjects.MuseumDTO>();
            DataAccessObjects.MuseumDao dao = new DataAccessObjects.MuseumDao();

            museumList = dao.List();
            return View(museumList);
        }

        /// <summary>
        /// This action opens new create page
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            Models.MuseumModel model = new Models.MuseumModel();
            return View(model);
        }

        /// <summary>
        /// This action saves DB
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(Models.MuseumModel model)
        {
            if (ModelState.IsValid)
            {
                DataAccessObjects.MuseumDao dao = new DataAccessObjects.MuseumDao();
                BusinessObjects.MuseumDTO dto = new BusinessObjects.MuseumDTO();
                /*I'm Converting MVC model to BusinessObject*/
                dto.Email = model.Email;
                dto.BillingAddress = model.BillingStreet + " " + model.BillingCity + " " + model.BillingPostCode;
                dto.ShippingAddress = model.ShippingStreet + " " + model.ShippingCity + " " + model.ShippingPostCode;
                Result result = dao.Insert(dto);
                /*if there is problem about saving I'll show you UserMessage; 
                if There is no problem I'll show MusseumList*/
                if (result.IsSucceeded)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.UserMessage = result.UserMessage;
                    return View(dto);
                }
            }

            return View();
        }

        /// <summary>
        /// Delete from DB
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult Delete(int id)
        {
            DataAccessObjects.MuseumDao dao = new DataAccessObjects.MuseumDao();
            BusinessObjects.MuseumDTO dto = new BusinessObjects.MuseumDTO();
            dto.MuseumID = id;
            Result result = dao.Delete(dto);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}