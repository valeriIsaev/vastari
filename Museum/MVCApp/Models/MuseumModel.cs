﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVCApp.Models
{
    public class MuseumModel
    {
        /// <summary>
        /// MuseumID
        /// </summary>
        public int MuseumID { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        [Required]
        [StringLength(255)]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }


        /// <summary>
        /// BillingStreet
        /// </summary>
        [Required(ErrorMessage = "BillingStreet is required")]
        [StringLength(255)]
        [Display(Name = "Street")]
        public string BillingStreet { get; set; }

        /// <summary>
        /// BillingCity
        /// </summary>
      
        [Required (ErrorMessage = "BillingCity is required")]
        [StringLength(255)]
        [Display(Name = "City")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "BillingCity use letters only please")]
        public string BillingCity { get; set; }

        /// <summary>
        /// BillingPostCode
        /// </summary>
        [Required(ErrorMessage = "BillingPostCode is required")]
        [StringLength(7)]
        [Display(Name = "PostCode")]     
        [RegularExpression(@"^[A-Za-z0-9_.]+$", ErrorMessage = "BillingPostCode use [A-Z][0-9] only please")]
        public string BillingPostCode { get; set; }

        /// <summary>
        /// ShippingStreet
        /// </summary>
        [Required(ErrorMessage = "ShippingStreet is required")]
        [StringLength(255)]
        [Display(Name = "Street")]
        public string ShippingStreet { get; set; }

        /// <summary>
        /// ShippingCity
        /// </summary>
        [Required(ErrorMessage = "ShippingCity is required")]
        [StringLength(255)]
        [Display(Name = "City")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "ShippingCity use letters only please")]
        public string ShippingCity { get; set; }

        /// <summary>
        /// ShippingPostCode
        /// </summary>
        [Required(ErrorMessage = "ShippingPostCode is required")]
        [StringLength(7)]
        [Display(Name = "PostCode")]
        [RegularExpression(@"^[A-Za-z0-9_.]+$", ErrorMessage = "ShippingPostCode use [A-Z][0-9] only please")]
        public string ShippingPostCode { get; set; }
    }
}