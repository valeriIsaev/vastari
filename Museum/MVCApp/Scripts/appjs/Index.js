﻿

$(".buttonDelete").button().click(function () {
    var selectedId = $(this).parents('tr:first').children('td:first').text().replace(/\n/g, "").trim();

    var reply = confirm("Are you sure want to delete!");
    if (reply == true) {
        deleteRecord(selectedId);
    }
});

var deleteRecord = function (selectedId)
{
    $.ajax({
        url: "/Home/Delete",
        data: { id: selectedId },
        type: "Post",
        success: function (result) {
            alert(result.UserMessage);
            /*if delete process is success I'll go MuseumList*/
            if (result.IsSucceeded) {
                window.location.href = "/Home/Index";
            }
        },
        error: function () {
            alert("something seems wrong");
        }
    });
}