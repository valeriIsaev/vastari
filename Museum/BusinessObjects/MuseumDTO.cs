﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObjects
{
    /// <summary>
    /// Domain Object Model Museum
    /// </summary>
    public class MuseumDTO
    {
        /// <summary>
        /// MuseumID
        /// </summary>
        public int MuseumID { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string BillingAddress { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public string ShippingAddress { get; set; }
    }
}


