﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessObjects
{
    public class BaseDao : IDisposable
    {
        public SqlHelper Helper;

        public BaseDao()
        {
            Helper = new SqlHelper();
        }

        private bool isDisposed = false;
        private void Dispose(bool isDisposed)
        {
            if (!this.isDisposed)
            {
                if (isDisposed)
                {
                    Helper.Dispose();
                }
            }

            isDisposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
