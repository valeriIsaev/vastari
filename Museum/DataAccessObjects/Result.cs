﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessObjects
{

    /// <summary>
    /// I return this object to UI side
    /// </summary>
    public class Result
    {
        /// <summary>
        /// IsSucceeded
        /// </summary>
        public bool IsSucceeded { get; set; }

        /// <summary>
        /// UserMessage
        /// </summary>
        public string UserMessage { get; set; }
    }
}
