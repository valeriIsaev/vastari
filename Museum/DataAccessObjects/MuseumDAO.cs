﻿using BusinessObjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessObjects
{
    /// <summary>
    /// Sql Server specific data access object that handles data access of Museum.
    /// </summary>
    public class MuseumDao : BaseDao
    {
        /// <summary>
        /// Insert
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public Result Insert(MuseumDTO obj)
        {
            Result result = new Result();

            try
            {
                SqlParameter[] param = {
                                           new SqlParameter("@MuseumID",obj.MuseumID),
                                           new SqlParameter("@Email",obj.Email),
                                           new SqlParameter("@BillingAddress",obj.BillingAddress),
                                           new SqlParameter("@ShippingAddress",obj.ShippingAddress)
                                       };

                result = Helper.Exec("Museum_Insert", param);
            }
            catch (Exception ex)
            {
                result.IsSucceeded = false;
                result.UserMessage = ex.ToString();
            }
            return result;
        }


        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public Result Delete(MuseumDTO obj)
        {
            Result result = new Result();

            try
            {
                SqlParameter[] param = {
                                           new SqlParameter("@MuseumId",obj.MuseumID),
                                       };

                result = Helper.Exec("Museum_Delete", param);
            }
            catch (Exception ex)
            {
                result.IsSucceeded = false;
                result.UserMessage = ex.ToString();
            }
            return result;
        }


        /// <summary>
        /// List
        /// </summary>
        /// <returns>Category list.</returns>
        public List<MuseumDTO> List()
        {
            return Helper.ReadList("Museum_List", Make);
        }

        /// <summary>
        /// Creates a Museum object based on DataReader.
        /// </summary>
        private static Func<IDataReader, MuseumDTO> Make = reader =>
           new MuseumDTO
           {
               MuseumID = reader["MuseumID"].AsId(),
               Email = reader["Email"].AsString(),
               BillingAddress = reader["BillingAddress"].AsString(),
               ShippingAddress = reader["ShippingAddress"].AsString()
           };

    }
}
