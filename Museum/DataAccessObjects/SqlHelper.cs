﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessObjects
{
    /// <summary>
    /// This class for CRUD operations
    /// </summary>
    public class SqlHelper
    {
        /// <summary>
        /// 
        /// </summary>
        string ConnectionString = ConfigurationManager.ConnectionStrings["museum"].ToString();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="spName"></param>
        /// <param name="sqlParamaters"></param>
        /// <returns></returns>
        public Result Exec(string spName, SqlParameter[] sqlParamaters)
        {
            Result result = new Result();

            using (SqlConnection dbConnection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand dbCommand = new SqlCommand(spName, dbConnection))
                {
                    using (SqlDataAdapter sqlAdapter = new SqlDataAdapter(dbCommand))
                    {
                        try
                        {
                            dbCommand.CommandType = CommandType.StoredProcedure;

                            dbCommand.Parameters.AddRange(sqlParamaters);
                            dbCommand.Parameters.Add("@UserMessage", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
                            dbCommand.Parameters.Add("@IsSucceeded", SqlDbType.Bit).Direction = ParameterDirection.Output;

                            dbCommand.Connection.Open();
                            dbCommand.ExecuteScalar();
                            dbCommand.Connection.Close();

                            result.IsSucceeded = Convert.ToBoolean(dbCommand.Parameters["@IsSucceeded"].Value.ToString());
                            result.UserMessage = dbCommand.Parameters["@UserMessage"].Value.ToString();

                        }
                        catch (Exception ex)
                        {
                            dbCommand.Connection.Close();
                            result.IsSucceeded = false;
                            result.UserMessage = ex.ToString();
                        }
                    }
                }
            }

            return result;
        }


        /// <summary>
        /// For Fast Read
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="make"></param>
        /// <param name="parms"></param>
        /// <returns></returns>
        public T Read<T>(string spName, Func<IDataReader, T> make, object[] parms = null)
        {
            using (SqlConnection dbConnection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand dbCommand = new SqlCommand(spName, dbConnection))
                {
                    dbCommand.CommandType = CommandType.StoredProcedure;
                    SetParameters(dbCommand, parms);

                    dbConnection.Open();

                    T t = default(T);
                    var reader = dbCommand.ExecuteReader();
                    if (reader.Read())
                        t = make(reader);

                    return t;
                }
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="spName"></param>
        /// <param name="make"></param>
        /// <param name="parms"></param>
        /// <returns></returns>
        public List<T> ReadList<T>(string spName, Func<IDataReader, T> make, object[] parms = null)
        {
            using (SqlConnection dbConnection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand dbCommand = new SqlCommand(spName, dbConnection))
                {
                    dbCommand.CommandType = CommandType.StoredProcedure;
                    SetParameters(dbCommand, parms);

                    dbConnection.Open();

                    var list = new List<T>();
                    var reader = dbCommand.ExecuteReader();

                    while (reader.Read())
                        list.Add(make(reader));

                    return list;
                }
            }
        }


        /// <summary>
        /// Adds parameters to command object.
        /// </summary>
        /// <param name="command">Command object.</param>
        /// <param name="parms">Array of name-value query parameters.</param>
        private void SetParameters(SqlCommand command, object[] parms)
        {
            if (parms != null && parms.Length > 0)
            {
                // NOTE: Processes a name/value pair at each iteration
                for (int i = 0; i < parms.Length; i += 2)
                {
                    string name = parms[i].ToString();

                    // No empty strings to the database
                    if (parms[i + 1] is string && (string)parms[i + 1] == "")
                        parms[i + 1] = null;

                    // If null, set to DbNull
                    object value = parms[i + 1] ?? DBNull.Value;

                    var dbParameter = command.CreateParameter();
                    dbParameter.ParameterName = name;
                    dbParameter.Value = value;

                    command.Parameters.Add(dbParameter);
                }
            }
        }


        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            this.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}